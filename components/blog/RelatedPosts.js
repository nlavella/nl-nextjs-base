import Link from "../Link";

export default function RelatedPosts({ posts }) {
	function formatDate(_date) {
		let _preDate = new Date(_date);
		return (
			_preDate.getMonth() +
			"." +
			_preDate.getDate() +
			"." +
			_preDate.getFullYear()
		);
	}

	return (
		<>
			{posts.map(({ node }) => (
				<div className="post" key={node.slug}>
					<Link href={"/blog/" + node.slug}>
						<a>{node.title}</a>
					</Link>
				</div>
			))}
		</>
	);
}
