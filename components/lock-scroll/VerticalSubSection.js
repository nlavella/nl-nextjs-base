import React, { useRef, useState, useEffect } from "react";

const VerticalSubSections = ({
  id,
  children,
  containerRef,
  isActive,
  progress,
}) => {
  const [percentScroll, setPercentScroll] = useState(0);
  const ref = useRef(null);
  const onScreen = useOnScreen(ref, "-10px");

  useEffect(() => {
    applyScrollListener(ref, containerRef);
  }, []);

  useEffect(() => {
    if (onScreen) isActive(id);
  }, [onScreen]);

  useEffect(() => {
    progress(percentScroll);
  }, [percentScroll]);

  const applyScrollListener = (ref, containerRef) => {
    containerRef.current.addEventListener("scroll", () => {
      const scrollHeight = containerRef.current.scrollTop + window.innerHeight;
      const sectionOffsetTop = ref.current.offsetTop;
      const sectionOverallHeight = ref.current.scrollHeight;

      //console.log(scrollHeight + ", " + sectionOffsetTop);

      if (
        scrollHeight >= sectionOffsetTop &&
        scrollHeight <= sectionOffsetTop + sectionOverallHeight
      ) {
        const percent = Math.round(
          ((scrollHeight - sectionOffsetTop) / sectionOverallHeight) * 100
        );
        setPercentScroll(percent);
      }
    });
  };

  return <div ref={ref}>{children}</div>;
};

function useOnScreen(containerRef, rootMargin = "0px") {
  const [isIntersecting, setIntersecting] = useState(false);

  useEffect(() => {
    const observer = new IntersectionObserver(
      ([entry]) => {
        setIntersecting(entry.isIntersecting);
      },
      { rootMargin }
    );

    if (containerRef.current) {
      observer.observe(containerRef.current);
    }

    return () => {
      observer.unobserve(containerRef.current);
    };
  }, []);

  return isIntersecting;
}

export default VerticalSubSections;
