import React, { useEffect, useRef, useState } from "react";

const VerticalSections = ({ children }) => {
  const [scrollPos, setScrollPos] = useState(0);
  const [currentSection, setCurrentSection] = useState(1);
  const [currentPercent, setCurrentPercent] = useState(1);
  const containerRef = useRef(null);

  const activeSection = (id) => {
    //console.log(children.length + ", " + id);
    setCurrentSection(id + 1);
  };

  const progress = (percentScroll) => {
    setCurrentPercent(percentScroll);
  };

  const scrollContainer = () => {
    const sectionHeight = containerRef.current.clientHeight;
    const scrollTo = scrollPos + sectionHeight;
    containerRef.current.scrollTop = scrollTo;
    setScrollPos(scrollTo);
  };

  const childrenWithProps = React.Children.map(children, (child) => {
    // checking isValidElement is the safe way and avoids a typescript error too
    if (React.isValidElement(child)) {
      return React.cloneElement(child, {
        containerRef: containerRef,
        isActive: activeSection,
        progress: progress,
      });
    }
    return child;
  });

  return (
    <div
      className="verticalSections"
      ref={containerRef}
      onClick={scrollContainer}
    >
      {childrenWithProps}
      <div className="indicator">
        <div className="slide">{currentSection}</div>
        <div className="percent">{currentPercent}%</div>
      </div>
    </div>
  );
};

export default VerticalSections;
