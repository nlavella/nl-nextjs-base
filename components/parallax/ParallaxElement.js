import React, { useState, useRef, useEffect } from "react";

/**
 *
 * INSTRUCTIONS
 * elements animated at speed and direction based on parent container and relation to viewport
 *
 * set SPEED to determine scale of move (0 will scroll normal)
 * set TOP, RIGHT, BOTTOM, LEFT for position
 * set XOFFSET to determine scale of x move (0 will scroll normal, - to left, + to right)
 * set YOFFSET to determine scale of y move (0 will scroll normal, - to left, + to right)
 * **/

const ParallaxElement = React.forwardRef(
	(
		{
			top,
			right,
			bottom,
			left,
			speed,
			xOffset,
			yOffset,
			containerRef,
			children,
			className,
		},
		ref
	) => {
		const [lastScroll, setLastScroll] = useState(0);
		const [translateX, setTranslateX] = useState(0);
		const [translateY, setTranslateY] = useState(0);

		useEffect(() => {
			applyScrollListener();
		}, []);

		const applyScrollListener = () => {
			window.addEventListener("scroll", () => {
				const scrollY =
					window.pageYOffset || document.documentElement.scrollTop;
				if (scrollY !== lastScroll) {
					setLastScroll(scrollY);
					handleParallax();
				}
			});

			window.addEventListener("resize", () => {
				setLastScroll(window.pageYOffset || document.documentElement.scrollTop);
				handleParallax();
			});
		};

		const handleParallax = () => {
			let viewportHeight = window.innerHeight;
			let speedAux = speed;

			let oy = lastScroll + containerRef.current.getBoundingClientRect().top;
			let deltaY = (viewportHeight - containerRef.current.scrollHeight) / 2.0;

			let pageOff = oy - deltaY;

			let _yOffset = yOffset;
			let ySpeed = -((speedAux * _yOffset + 25) / 1000.0);
			let yDistance = (lastScroll - pageOff) * 1 * ySpeed;
			setTranslateY(yDistance);

			let _xOffset = xOffset;
			let xSpeed = -((speedAux * _xOffset + 25) / 1000.0);
			let xDistance = (lastScroll - pageOff) * 1 * xSpeed;
			setTranslateX(xDistance);
		};

		return (
			<div
				className={className}
				style={{
					transform: `translate3d(${translateX}px, ${translateY}px, 0)`,
					top: `${top}%`,
					left: `${left}%`,
					right: `${right}%`,
					bottom: `${bottom}%`,
				}}>
				{children}
			</div>
		);
	}
);

export default ParallaxElement;
