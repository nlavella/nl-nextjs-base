import React, { useState, useRef, useEffect } from "react";

const ParallaxSection = ({ id, children, inViewport }) => {
  const containerRef = useRef(null);
  const onScreen = useOnScreen(containerRef, "-100px");

  const childrenWithProps = React.Children.map(children, (child) => {
    // checking isValidElement is the safe way and avoids a typescript error too
    if (React.isValidElement(child)) {
      return React.cloneElement(child, { containerRef: containerRef });
    }
    return child;
  });

  return (
    <section
      className={onScreen ? "parallax-section active" : "parallax-section"}
      id={id}
      ref={containerRef}
    >
      {childrenWithProps}
    </section>
  );
};

function useOnScreen(containerRef, rootMargin = "0px") {
  const [isIntersecting, setIntersecting] = useState(false);

  useEffect(() => {
    const observer = new IntersectionObserver(
      ([entry]) => {
        setIntersecting(entry.isIntersecting);
      },
      { rootMargin }
    );

    if (containerRef.current) {
      observer.observe(containerRef.current);
    }

    return () => {
      observer.unobserve(containerRef.current);
    };
  }, []);

  return isIntersecting;
}

export default ParallaxSection;
