import { useState, useEffect } from "react";
import { useRouter } from "next/router";

import Link from "../routing/Link";
import AnchorLink from "../routing/AnchorLink";

const Nav = (props) => {
	const menuItems = props.menuItems;

	const router = useRouter();
	const [showMenu, setShowMenu] = useState(false); //mobile only

	useEffect(() => {}, []);

	useEffect(() => {
		// anytime path changes, close nav & subnav
		setShowMenu(false);
	}, [router.asPath]);

	return (
		<nav className={showMenu ? "menu-active" : ""}>
			<div className="mobile-toggle " onClick={() => setShowMenu(!showMenu)}>
				<div className="line"></div>
				<div className="line"></div>
				<div className="line"></div>
			</div>
			<ul>
				{menuItems.length > 0 &&
					menuItems.map((item, index) => (
						<li key={index}>
							<Link href={item.node.path.slice(0, -1)}>
								<a>{item.node.label}</a>
							</Link>
						</li>
					))}
			</ul>
		</nav>
	);
};

export default Nav;
