import { useState } from "react";

const Accordian = ({ title, children }) => {
  const [open, setOpen] = useState(false);

  return (
    <div className={open ? "accordian-item open" : "accordian-item"}>
      <div className="accordian-header" onClick={() => setOpen(!open)}>
        <div className="accordian-icon">
          <span className="open-icon">+</span>
          <span className="close-icon">-</span>
        </div>
        {title}
      </div>
      <div className="accordian-details">{children}</div>
    </div>
  );
};

export default Accordian;
