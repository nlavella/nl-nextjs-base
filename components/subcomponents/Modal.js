import { Fragment, useState } from "react";

const Modal = ({ title }) => {
  const [visible, setVisible] = useState(false);

  return (
    <Fragment>
      <a onClick={() => setVisible(true)}>{title}</a>
      <div className={visible ? "modal active" : "modal"}>
        <div className="modalContent">
          <div className="close" onClick={() => setVisible(false)}>
            &times;
          </div>
          <h2>Modal</h2>
          <p>Content!</p>
        </div>
      </div>
    </Fragment>
  );
};

export default Modal;
