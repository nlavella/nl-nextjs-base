import { useRef, useState, useEffect } from "react";

const AnimateSvgator = ({ file, isOnscreen }) => {
  const ref = useRef(null);

  useEffect(() => {
    if (isOnscreen && ref) {
      if (ref.current.contentDocument) {
        var svg = ref.current.contentDocument.querySelector("svg");
        if (svg) svg.dispatchEvent(new Event("click"));
      }
    }
  }, [isOnscreen]);

  return <object type="image/svg+xml" data={file} ref={ref}></object>;
};

export default AnimateSvgator;
