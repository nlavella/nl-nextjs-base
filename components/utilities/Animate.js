import React, { useState, useRef, useEffect } from "react";

const Animate = ({ children, className, id, percentVisible, hold }) => {
	const [visibleOffset, setVisibleOffset] = useState("-100px");
	const [held, setHeld] = useState(false); //default holds once loaded

	const containerRef = useRef(null);
	const onScreen = useOnScreen(containerRef, percentVisible);

	//hold onscreen
	useEffect(() => {
		if (hold !== false && onScreen) setHeld(true);
	}, [onScreen]);

	return (
		<div
			className={onScreen || held ? "active " + className : className}
			id={id}
			ref={containerRef}>
			{children}
		</div>
	);
};

function useOnScreen(containerRef, threshold = 0.5) {
	const [isIntersecting, setIntersecting] = useState(false);

	useEffect(() => {
		const observer = new IntersectionObserver(
			([entry]) => {
				setIntersecting(entry.isIntersecting);
			},
			{ root: null, rootMargin: "0px", threshold: threshold }
		);

		if (containerRef.current) {
			observer.observe(containerRef.current);
		}

		return () => {
			//observer.unobserve(containerRef.current);
			observer.disconnect();
		};
	}, []);

	/*
  ANIMATION TROUBLESHOOTING - Todo: rework intersection observer for % + thresholds

  function useOnScreen(containerRef, threshold = 0.5) {
	const [isIntersecting, setIntersecting] = useState(false);

	useEffect(() => {
		const observer = new IntersectionObserver(
			([entry]) => {
				setIntersecting(entry.isIntersecting);
			},
			{ root: null, rootMargin: "0px", threshold: threshold }
		);

		if (containerRef.current) {
			observer.observe(containerRef.current);
		}

		return () => {
			//observer.unobserve(containerRef.current);
			observer.disconnect();
		};
	}, []);
*/

	return isIntersecting;
}

export default Animate;
