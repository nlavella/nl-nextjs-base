// Scroll status: calculates percent of section scrolled into viewport, WIP

import React, { useRef, useState, useEffect } from "react";

const ScrollStatus = ({ children }) => {
	const [percentScroll, setPercentScroll] = useState(0);
	const ref = useRef(null);

	useEffect(() => {
		applyScrollListener(ref);
	}, []);

	const applyScrollListener = (ref) => {
		window.addEventListener("scroll", () => {
			const scrollPosition = window.pageYOffset + window.innerHeight;
			const sectionOverallHeight = ref.current.scrollHeight;

			//console.log(scrollPosition + ", " + sectionOverallHeight);

			const percent = Math.round((scrollPosition / sectionOverallHeight) * 100);

			console.log(percent);
		});
	};

	return (
		<div className="scrollStatus" ref={ref}>
			{children}
		</div>
	);
};

export default ScrollStatus;
