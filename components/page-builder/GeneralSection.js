const GeneralSection = ({ content }) => {
	return (
		<div
			className="general-section pb-section"
			dangerouslySetInnerHTML={{ __html: content.content }}></div>
	);
};

export default GeneralSection;
