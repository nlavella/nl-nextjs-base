const ThirdGraphicSection = ({ content }) => {
	return (
		<div className="third-graphic-section pb-section">
			<div className="img">
				{content.graphic && <img src={content.graphic.mediaItemUrl} />}
			</div>
			{content.caption && (
				<div
					className="caption"
					dangerouslySetInnerHTML={{ __html: content.caption }}></div>
			)}
		</div>
	);
};

export default ThirdGraphicSection;
