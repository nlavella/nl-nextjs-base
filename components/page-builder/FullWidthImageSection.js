const FullWidthImageSection = ({ content }) => {
	return (
		<div className="full-width-image-section pb-section">
			{content.image && <img src={content.image.mediaItemUrl} />}
			{content.caption && <p>{content.caption}</p>}
		</div>
	);
};

export default FullWidthImageSection;
