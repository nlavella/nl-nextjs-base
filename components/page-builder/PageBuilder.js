import GeneralSection from "./GeneralSection";
import FullWidthImageSection from "./FullWidthImageSection";
import ThirdGraphicSection from "./ThirdGraphicSection";

const PageBuilder = ({ sections }) => {
	function renderSwitch(section) {
		switch (section.fieldGroupName) {
			case "Post_Blogpost_Section_GeneralContent":
				return <GeneralSection content={section} />;
			case "Post_Blogpost_Section_FullWidthImage":
				return <FullWidthImageSection content={section} />;
			case "Post_Blogpost_Section_ThirdsGraphic":
				return <ThirdGraphicSection content={section} />;
			default:
				return NULL;
		}
	}

	return (
		<>
			{sections.length > 0 &&
				sections.map((node, index) => (
					<div key={index}>{renderSwitch(node)}</div>
				))}
		</>
	);
};

export default PageBuilder;
