import { useState, useRef, useEffect } from "react";

const HorizontalScroll = ({ children }) => {
  const [dynamicHeight, setDynamicHeight] = useState(null);
  const [translateX, setTranslateX] = useState(0);

  const containerRef = useRef(null);
  const objectRef = useRef(null);

  useEffect(() => {
    handleDynamicHeight(objectRef);
    window.addEventListener("resize", resizeHandler);
    applyScrollListener(containerRef);
  }, []);

  const handleDynamicHeight = (ref) => {
    const objectWidth = ref.current.scrollWidth;
    const _dynamicHeight =
      objectWidth - window.innerWidth + window.innerHeight + 150;
    setDynamicHeight(_dynamicHeight);
  };

  const resizeHandler = () => {
    handleDynamicHeight(objectRef);
  };

  const applyScrollListener = (ref) => {
    window.addEventListener("scroll", () => {
      const offsetTop = -ref.current.offsetTop;
      setTranslateX(offsetTop);
    });
  };

  return (
    <div className="tallOuterContainer" style={{ height: dynamicHeight }}>
      <div className="stickyInnerContainer" ref={containerRef}>
        <div
          className="horizontalTranslateContainer"
          style={{ transform: `translate3d(${translateX}px, 0, 0)` }}
          ref={objectRef}
        >
          {children}
        </div>
      </div>
    </div>
  );
};

export default HorizontalScroll;
