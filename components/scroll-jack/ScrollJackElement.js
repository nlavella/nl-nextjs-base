import { useState, useRef, useEffect } from "react";

const ScrollJack = ({
	className,
	children,
	containerRef,
	start,
	end,
	startMarker,
	stopMarker,
	mobile,
}) => {
	const [styles, setStyles] = useState({ opacity: "1", transform: "" });
	const [current, setCurrent] = useState({
		x: 0,
		y: 0,
		s: 1,
		opacity: 1,
	});

	useEffect(() => {
		window.addEventListener("resize", resizeHandler);
		applyScrollListener(containerRef);
		setCurrent({
			x: start.x * window.innerWidth,
			y: start.y * window.innerHeight,
			s: start.s,
			opacity: start.opacity,
		});
	}, []);

	// on resize recalc
	const resizeHandler = () => {
		findPercentShowing(ref.current);
	};

	// on scroll calc
	const applyScrollListener = (ref) => {
		window.addEventListener("scroll", () => {
			findPercentShowing(ref.current);
		});
	};

	// find percent
	const findPercentShowing = (ref) => {
		//if based on window
		let rect = ref.getBoundingClientRect();

		// check if we're in viewport
		if (rect.top <= window.innerHeight && rect.bottom > 0) {
			var showing = Math.abs(rect.top - window.innerHeight);
			var percentShowing = parseFloat(showing / rect.height);

			//if in viewport on load start animation at bottom of section
			/*if (first) {
				showing -= window.innerHeight;
			}*/

			if (percentShowing >= startMarker && percentShowing <= stopMarker) {
				jackCalc(percentShowing);
			}

			console.log(showing + ", " + percentShowing);
		}
	};

	// apply styles anytime current is updated
	useEffect(() => {
		// set styles
		if (mobile || (!mobile && window.innerWidth > 960)) {
			setStyles({
				opacity: current.opacity,
				transform:
					"translate3d(" +
					current.x +
					"px," +
					current.y +
					"px,0) scale(" +
					current.s +
					")",
			});
		} else {
			setStyles({
				opacity: start.opacity,
				transform:
					"translate3d(" +
					start.x +
					"px," +
					start.y +
					"px,0) scale(" +
					start.s +
					")",
			});
		}
	}, [current]);

	// do location math
	const jackCalc = (percentShowing) => {
		console.log("move me:" + percentShowing);

		let jackPercent =
			(percentShowing - startMarker) / (stopMarker - startMarker);

		let _x = start.x + (end.x - start.x) * jackPercent;
		let _y = start.y + (end.y - start.y) * jackPercent;
		let _s = start.s + (end.s - start.s) * jackPercent;
		let _opacity = start.opacity + (end.opacity - start.opacity) * jackPercent;

		// store current
		setCurrent({
			x: _x * window.innerWidth,
			y: _y * window.innerHeight,
			s: _s,
			opacity: _opacity,
		});
	};

	return (
		<div className={"element " + className} style={styles}>
			{children}
		</div>
	);
};

export default ScrollJack;
