import Head from "next/head";

import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { scroller } from "react-scroll";

import Header from "../../sections/Header";
import Footer from "../../sections/Footer";

const Page = ({ children }) => {
	const router = useRouter();
	const [headerOffset, setOffset] = useState(0);
	const anchor = router.asPath.split("#")[1];

	const trimPage = function (page) {
		return page.split("/")[0] == "" ? page.split("/")[1] : page.split("/")[0];
	};

	const pageTitle = trimPage(router.pathname)
		? trimPage(router.pathname)
		: "index";
	const className = "page-" + pageTitle;

	useEffect(() => {
		// scroll anchor if present
		{
			if (anchor) {
				scroller.scrollTo(anchor, {
					duration: 800,
					offset: 0,
					smooth: "easeInOut",
				});
			}
		}
	}, [anchor]);

	return (
		<div className={className}>
			<Head>
				{/*<script
					async
					src="https://www.googletagmanager.com/gtag/js?id=UA-21482175-1"
					></script>
					<script
					dangerouslySetInnerHTML={{
						__html: `window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-21482175-1');`,
					}}
					/>*/}
				<link rel="icon" type="image/x-icon" href="/favicon.ico" />
			</Head>
			<Header />
			<main>{children}</main>
			<Footer />
		</div>
	);
};

export default Page;
