import React, { Fragment, useEffect, useState } from "react";
import { Router, useRouter } from "next/router";
import Link from "next/link";
import { Link as SmoothLink } from "react-scroll";

const AnchorLink = ({ href, title, closeNav }) => {
	const router = useRouter();
	const [headerOffset, setOffset] = useState(0);

	useEffect(() => {
		// set offset for scroll and padding
		function manageHeaderOffset() {
			setOffset(document.querySelector("header").offsetHeight);
		}

		window.addEventListener("resize", manageHeaderOffset);

		manageHeaderOffset();

		return () => window.removeEventListener("resize", manageHeaderOffset);
	}, []);

	const localLink = (
		<SmoothLink
			to={href.split("#")[1]}
			smooth={true}
			duration={1500}
			offset={-headerOffset}
			spy={true}
			hashSpy={true}
			onClick={closeNav}>
			{title}
		</SmoothLink>
	);

	const refreshLink = (
		<Link href={href}>
			<a>{title}</a>
		</Link>
	);

	return (
		<Fragment>
			{router.asPath.split("#")[0] === href.split("#")[0]
				? localLink
				: refreshLink}
		</Fragment>
	);
};

export default AnchorLink;
