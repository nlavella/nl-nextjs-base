import React from "react";
import Link from "next/link";
import { withRouter } from "next/router";
import { Children } from "react";

const ActiveLink = withRouter(({ router, children, ...props }) => (
	<Link {...props}>
		{React.cloneElement(Children.only(children), {
			className:
				router.asPath.split("#")[0] === props.href
					? `active ` + props.className
					: props.className,
		})}
	</Link>
));

export default ActiveLink;
