const API_URL = "WPURL/graphql/";

async function fetchAPI(query, { variables } = {}) {
	const headers = { "Content-Type": "application/json" };

	const res = await fetch(API_URL, {
		method: "POST",
		headers,
		body: JSON.stringify({
			query,
			variables,
		}),
	});

	const json = await res.json();
	if (json.errors) {
		console.error(json.errors);
		throw new Error("Failed to fetch API");
	}
	return json.data;
}

export async function getAllPostsWithSlug() {
	const data = await fetchAPI(`
    {
      posts(first: 10000) {
        edges {
          node {
            slug
          }
        }
      }
    }
  `);
	return data?.posts;
}

export async function getAllCategories() {
	const data = await fetchAPI(
		`
      {
        categories(where: {hideEmpty: true}) {
          nodes {
            name
            slug
          }
        }
      }
      `
	);

	return data?.categories;
}

export async function getAllPosts() {
	const data = await fetchAPI(
		`
    query AllPosts {
      posts(first: 10000, where: { orderby: { field: DATE, order: DESC } }) {
        edges {
          node {
            title
            slug
            date
            blogPost {
              excerpt
              thumbnail {
                mediaItemUrl
              }
            }
          }
        }
      }
    }
  `
	);

	return data?.posts;
}

export async function getAllPostsByCategory(slug) {
	const data = await fetchAPI(
		`query PostsByCategory($slug: String!) {
      posts(where: {categoryName: $slug}) {
        edges {
          node {
            title
            slug
            date
            blogPost {
              excerpt
              thumbnail {
                mediaItemUrl
              }
            }
          }
        }
      }
    }
  `,
		{
			variables: {
				slug: slug,
			},
		}
	);

	return data?.posts;
}

export async function getPostAndMorePosts(slug) {
	const data = await fetchAPI(
		`query PostBySlug($id: ID!, $idType: PostIdType!) {
      post(id: $id, idType: $idType) {
        title
        slug
        date
        categories {
          edges {
            node {
              name
            }
          }
        }
        seo {
          title
          metaDesc
        }
        blogPost {
          section {
            ... on Post_Blogpost_Section_ThirdsGraphic {
              caption
              fieldGroupName
              graphic {
                mediaItemUrl
              }
            }
            ... on Post_Blogpost_Section_FullWidthImage {
              caption
              fieldGroupName
              image {
                mediaItemUrl
              }
            }
            ... on Post_Blogpost_Section_GeneralContent {
              content
              fieldGroupName
            }
          }
        }
      }

      posts(first: 5, where: { orderby: { field: DATE, order: DESC } }) {
        edges {
          node {
            title
            slug
            date
            blogPost {
              excerpt
              thumbnail {
                mediaItemUrl
              }
            }
          }
        }
      }
    }
  `,
		{
			variables: {
				id: slug,
				idType: "SLUG",
			},
		}
	);

	// Filter out the main post
	data.posts.edges = data.posts.edges.filter(({ node }) => node.slug !== slug);
	// If there are still 5 posts, remove the last one
	if (data.posts.edges.length > 5) data.posts.edges.pop();

	return data;
}

export async function getGlobal() {
	const data = await fetchAPI(
		`query Global {
        globalFields {
          global {
            logo {
              mediaItemUrl
            }
          }
        }
        
        menu(id: "dGVybToy") {
        id
        menuItems {
          edges {
            node {
              id
              url
              order
              label
              path
            }
          }
        }
      }
    }
    `
	);

	return data;
}

export async function getAboutPage(id) {
	const data = await fetchAPI(
		`query About($id: ID!) {
      page(id: $id) {
        id
        template {
          ... on AboutTemplate {
            templateName
            aboutUs {
              valuesSection {
                headline
                values {
                  description
                  title
                }
                image{
                  mediaItemUrl
                }
              }
              teamSection {
                teamMembers {
                  position
                  name
                  headshot {
                    mediaItemUrl
                  }
                }
                headline
              }
              leadership {
                image {
                  mediaItemUrl
                }
                headshot {
                  mediaItemUrl
                }
                bio
                headline
              }
              introSection {
                callout
                content
                videoEmbed
              }
            }
            hero {
              fieldGroupName
              title
              backgroundImage {
                mediaItemUrl
              }
            }
          }
        }
      }
    }
    `,
		{
			variables: {
				id: id,
			},
		}
	);

	return data?.page.template;
}
