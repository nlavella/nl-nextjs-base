import Head from "next/head";

const contact = () => {
	return (
		<div className="contact">
			<Head>
				<title>Contact</title>
				<meta name="description" content="" />
			</Head>

			<section className="contact-form">
				<div className="cont">
					<form
						method="POST"
						action="/thank-you"
						netlify="contact-us"
						name="contact-us"
						netlify-honeypot="bot-field">
						<input name="bot-field" style={{ display: "none" }} />
						<input type="hidden" name="form-name" value="contact-us" />
						<div className="checkbox">
							<input
								type="checkbox"
								id="lets-chat"
								name="lets-chat"
								value="true"
							/>
							<label htmlFor="lets-chat">
								<span className="purple">Let's chat.</span>
							</label>
						</div>
						<input type="text" name="name" placeholder="Name" required />
						<input type="text" name="email" placeholder="Email" required />
						<input type="text" name="company_name" placeholder="Company name" />
						<input type="text" name="phone" placeholder="Phone (optional)" />
						<textarea name="message" placeholder="Your message"></textarea>
						<input type="submit" value="Submit" />
					</form>
				</div>
			</section>
		</div>
	);
};

export default contact;
