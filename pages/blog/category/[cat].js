import Head from "next/head";
import ErrorPage from "next/error";
import Animate from "../../../components/Animate";

import Link from "../../../components/routing/Link";
import MorePosts from "../../../components/MorePosts";
import {
	getAllCategories,
	getAllPostsByCategory,
} from "../../../lib/wordpress/api";

export default function Category({ allPostsByCategory, allCategories, slug }) {
	const categories = allCategories;

	return (
		<div className="blog">
			<Head>
				<title>Our Blog</title>
				<meta name="description" content="" />
			</Head>

			<Animate className="hero">
				<div className="full">
					<div className="cont">
						<h1>
							<span className="grey">Our Blog</span>
						</h1>
					</div>
				</div>
			</Animate>
			<section className="full" id="posts">
				<div className="cont">
					<img src="/assets/icons/Rhoads-3-Dots.svg" className="dots" />
					<div className="blog-cont">
						<div className="main">
							{allPostsByCategory && allPostsByCategory.edges.length > 0 && (
								<MorePosts posts={allPostsByCategory.edges} />
							)}
						</div>
						<div className="sidebar">
							<p>Categories</p>
							<div className="cats">
								{allCategories && allCategories.nodes.length > 0 ? (
									allCategories.nodes.map((cat, index) => (
										<Link
											href={"/blog/category/" + cat.slug}
											key={index}
											className={cat.name === slug ? "active" : ""}>
											<a>{cat.name}</a>
										</Link>
									))
								) : (
									<p>No categories found.</p>
								)}
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	);
}

export async function getStaticProps({ params }) {
	const allPostsByCategory = await getAllPostsByCategory(params.cat);
	const allCategories = await getAllCategories();
	const slug = params.cat;

	return {
		props: {
			allPostsByCategory,
			allCategories,
			slug,
		},
	};
}

export async function getStaticPaths() {
	const allCategories = await getAllCategories();

	return {
		paths:
			allCategories.nodes.map((node) => `/blog/category/${node.slug}`) || [],
		fallback: false,
	};
}
