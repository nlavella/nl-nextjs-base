import { useRouter } from "next/router";
import Head from "next/head";
import ErrorPage from "next/error";
import Animate from "../../components/Animate";

import Link from "../../components/routing/Link";
import MorePosts from "../../components/MorePosts";
import { getAllPosts, getAllCategories } from "../../lib/wordpress/api";

export default function Blog({ allPosts: { edges }, allCategories }) {
	const morePosts = edges;
	const categories = allCategories;

	return (
		<div className="blog">
			<Head>
				<title>Our Blog</title>
				<meta name="description" content="" />
			</Head>

			<section className="full" id="posts">
				<div className="cont">
					<img src="/assets/icons/Rhoads-3-Dots.svg" className="dots" />
					<div className="blog-cont">
						<div className="main">
							{morePosts.length > 0 && <MorePosts posts={morePosts} />}
						</div>
						<div className="sidebar">
							<p>Categories</p>
							<div className="cats">
								{allCategories.nodes.length > 0 ? (
									allCategories.nodes.map((cat, index) => (
										<Link href={"/blog/category/" + cat.slug} key={index}>
											<a>{cat.name}</a>
										</Link>
									))
								) : (
									<p>No categories found.</p>
								)}
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	);
}

export async function getStaticProps() {
	const allPosts = await getAllPosts();
	const allCategories = await getAllCategories();

	return {
		props: { allPosts, allCategories },
	};
}
