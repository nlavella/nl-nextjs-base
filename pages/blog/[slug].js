import { useRouter } from "next/router";
import Head from "next/head";
import ErrorPage from "next/error";
import Animate from "../../components/utilities/Animate";

import PageBuilder from "../../components/page-builder/PageBuilder";
import Link from "../../components/routing/Link";
import RelatedPosts from "../../components/blog/RelatedPosts";
import {
	getAllPostsWithSlug,
	getPostAndMorePosts,
	getAllCategories,
} from "../../lib/wordpress/api";

export default function Post({ post, posts, allCategories }) {
	const router = useRouter();
	const morePosts = posts?.edges;
	const categories = allCategories;

	console.log(post);

	if (!router.isFallback && !post?.slug) {
		return <ErrorPage statusCode={404} />;
	}

	function formatDate(_date) {
		let _preDate = new Date(_date);
		return (
			_preDate.getMonth() +
			"." +
			_preDate.getDate() +
			"." +
			_preDate.getFullYear()
		);
	}

	return (
		<div className="blog">
			<Head>
				<title>Blog</title>
				<meta name="description" content="" />
			</Head>

			<section className="full" id="posts">
				<div className="cont">
					<div className="blog-cont">
						{post && (
							<div className="main blog-post">
								<h1>{post.title}</h1>
								<p className="date">Posted {formatDate(post.date)}</p>
								<PageBuilder sections={post.blogPost.section} />
							</div>
						)}
						<div className="sidebar">
							<p>Categories</p>
							<div className="cats">
								{
									//{post.categories.edges[0].node.name}
									allCategories && allCategories.nodes.length > 0 ? (
										allCategories.nodes.map((cat, index) => (
											<Link href={"/blog/category/" + cat.slug} key={index}>
												<a>{cat.name}</a>
											</Link>
										))
									) : (
										<p>No categories found.</p>
									)
								}
							</div>
							<div className="recent-post">
								<p className="tlte">Recent Posts</p>
								{morePosts && morePosts.length > 0 && (
									<RelatedPosts posts={morePosts} />
								)}
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
	);
}

export async function getStaticProps({ params }) {
	const data = await getPostAndMorePosts(params.slug);
	const allCategories = await getAllCategories();

	return {
		props: {
			post: data.post,
			posts: data.posts,
			allCategories,
		},
	};
}

export async function getStaticPaths() {
	const allPosts = await getAllPostsWithSlug();

	return {
		paths: allPosts.edges.map(({ node }) => `/blog/${node.slug}`) || [],
		fallback: false,
	};
}
