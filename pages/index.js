import Head from "next/head";

// general components
import Link from "../components/routing/Link";
import AnchorLink from "../components/routing/AnchorLink";

// specalized sections
import HorizontalScroll from "../components/scroll-jack/HorizontalScroll";
import ScrollJackSection from "../components/scroll-jack/ScrollJackSection";
import ScrollJackElement from "../components/scroll-jack/ScrollJackElement";
import ParallaxSection from "../components/parallax/ParallaxSection";
import ParallaxElement from "../components/parallax/ParallaxElement";

// sub-components
import Accordian from "../components/subcomponents/Accordian";
import Modal from "../components/subcomponents/Modal";

// utilities
import Animate from "../components/utilities/Animate";

// slider
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Navigation, Pagination } from "swiper";
SwiperCore.use([Navigation, Pagination]);
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";

// headless
import { getHomepage, getGlobal } from "../lib/wordpress/api";

const home = () => {
	return (
		<div className="homepage">
			<Head>
				<title>Page title</title>
				<meta name="description" content="my description" />
			</Head>

			<div className="base-page">
				<section>
					<div className="cont">
						<h1>Homepage</h1>
						<ul>
							<li>
								<AnchorLink href="/#section1" title="Section 1" />
							</li>
							<li>
								<AnchorLink href="/about#about" title={"About Anchor"} />
							</li>
							<li>
								<Link href="/about">
									<a>About</a>
								</Link>
							</li>
							<li>
								<Link href="/lock-scroll">
									<a>
										Lock Scroll: Scroll snap sections with progress indicator
									</a>
								</Link>
							</li>
							<li>
								<Link href="/blog">
									<a>Blog: WP Headless Blog</a>
								</Link>
							</li>
							<li>
								<Link href="/contact">
									<a>Contact: Netlify contact form</a>
								</Link>
							</li>
						</ul>
					</div>
				</section>

				<section>
					<div className="cont">
						<p className="eyebrow">Eyebrow</p>
						<h1>H1: Style Guide</h1>
						<h2>H2: Style Guide</h2>
						<h3>H3: Style Guide</h3>
						<h4>H4: Style Guide</h4>
						<h5>H5: Style Guide</h5>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc sed
							porta ex. Fusce nec ante augue. Proin laoreet convallis orci vel
							mattis. Integer sodales ante ac dignissim tincidunt. Cras luctus
							lobortis turpis non eleifend. Nullam mollis volutpat arcu. Duis
							quis orci vel justo fringilla sollicitudin ac ac sem. Ut interdum
							purus est, eu faucibus ligula convallis eu. Cras elementum, risus
							nec hendrerit egestas, nibh ante pulvinar nulla, a aliquam mi
							velit sit amet lacus.
						</p>
						<ul className="bullets">
							<li>Cras elementum, risus nec hendrerit egestas</li>
							<li>nibh ante pulvinar nulla</li>
							<li>aliquam mi velit sit amet lacus.</li>
						</ul>
						<p className="disclaimer">** Disclaimer text</p>
						<a href="#" className="btn">
							Button
						</a>
					</div>
				</section>

				<section>
					<div className="cont">
						<h1>Sub-components</h1>
						<Accordian title="Accordian">Content</Accordian>
						<Modal title="Modal" />
					</div>
				</section>

				<ScrollJackSection>
					<ScrollJackElement
						start={{
							x: 0,
							y: 0,
							s: 1,
							opacity: 1,
						}}
						end={{
							x: 0.5,
							y: 0,
							s: 1,
							opacity: 1,
						}}
						startMarker={0.5}
						stopMarker={1}
						mobile={true}>
						To the right
					</ScrollJackElement>
					<ScrollJackElement
						start={{
							x: 1,
							y: 0,
							s: 1,
							opacity: 1,
						}}
						end={{
							x: 0.5,
							y: 0,
							s: 1,
							opacity: 1,
						}}
						startMarker={0.5}
						stopMarker={1}
						mobile={true}
						className="right">
						To the left
					</ScrollJackElement>
				</ScrollJackSection>

				<Animate
					className="section demo-animation"
					hold={false}
					percentVisible={0.5}>
					<div className="cont">
						<h1>Animate</h1>
					</div>
				</Animate>

				<Animate
					className="section demo-animation"
					hold={true}
					percentVisible={0.75}>
					<div className="cont">
						<h1>Animate - hold</h1>
					</div>
				</Animate>

				<ParallaxSection id={"section1"}>
					<ParallaxElement
						className="accent-layer left"
						speed={0.25}
						xOffset={-500}
						yOffset={500}
						right={5}
						bottom={10}>
						xxx
					</ParallaxElement>
					<ParallaxElement
						className="accent-layer right"
						speed={1}
						xOffset={100}
						yOffset={200}
						right={5}
						bottom={10}>
						xxx
					</ParallaxElement>
					<ParallaxElement
						speed={0}
						xOffset={0}
						yOffset={0}
						className="main-layer">
						<h2>Headline</h2>
					</ParallaxElement>
				</ParallaxSection>

				<HorizontalScroll>
					<div className="slideContainer">
						<div className="slide">Slide</div>
						<div className="slide">Slide</div>
						<div className="slide">Slide</div>
						<div className="slide">Slide</div>
					</div>
				</HorizontalScroll>

				<section className="">
					<div className="cont">
						<div className="swiper-cont">
							<Swiper
								slidesPerView={1}
								loop
								navigation
								pagination={{ clickable: true }}>
								<SwiperSlide>#1</SwiperSlide>
								<SwiperSlide>#2</SwiperSlide>
							</Swiper>
						</div>
					</div>
				</section>
			</div>
		</div>
	);
};

export default home;

/*
export async function getStaticProps() {
	//const content = await getHomepage("cG9zdDoxNA==");
	const global = await getGlobal();

	return {
		props: { content },
	};
}
*/
