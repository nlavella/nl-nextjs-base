import Page from "../components/routing/Page";
import "../styles/main.scss";

function MyApp({ Component, pageProps }) {
	return (
		<Page>
			<Component {...pageProps} />
		</Page>
	);
}

export default MyApp;
