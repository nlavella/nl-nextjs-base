import Link from "next/link";
import Head from "next/head";

import VerticalSections from "../components/lock-scroll/VerticalSections";
import VerticalSubSections from "../components/lock-scroll/VerticalSubSection";

export default function LockScroll() {
	return (
		<div className="homepage">
			<Head>
				<title>Page title</title>
				<meta name="description" content="my description" />
			</Head>

			<div className="base-page">
				<VerticalSections>
					<VerticalSubSections id={0}>
						<section>
							<div className="cont">
								<h1>page 1</h1>
							</div>
						</section>
						<section>
							<div className="cont">
								<h1>page 2</h1>
							</div>
						</section>
						<section>
							<div className="cont">
								<h1>page 3</h1>
							</div>
						</section>
					</VerticalSubSections>
					<VerticalSubSections id={1}>
						<section>
							<div className="cont">
								<h1>page 4</h1>
							</div>
						</section>
						<section>
							<div className="cont">
								<h1>page 5</h1>
							</div>
						</section>
						<section>
							<div className="cont">
								<h1>page 6</h1>
							</div>
						</section>
					</VerticalSubSections>
					<VerticalSubSections id={2}>
						<section>
							<div className="cont">
								<h1>page 7</h1>
							</div>
						</section>
						<section>
							<div className="cont">
								<h1>page 8</h1>
							</div>
						</section>
						<section>
							<div className="cont">
								<h1>page 9</h1>
							</div>
						</section>
					</VerticalSubSections>
				</VerticalSections>
			</div>
		</div>
	);
}
