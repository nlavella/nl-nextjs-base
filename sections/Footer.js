import { Fragment } from "react";

import Link from "../components/routing/Link";

const Footer = () => {
	return (
		<Fragment>
			<footer className="full">
				<div className="container">Footer</div>
			</footer>
		</Fragment>
	);
};

export default Footer;
