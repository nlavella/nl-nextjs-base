import { useState, useEffect } from "react";
import { useRouter } from "next/router";

import Link from "../components/routing/Link";
import AnchorLink from "../components/routing/AnchorLink";

const Nav = () => {
	const router = useRouter();
	const [showMenu, setShowMenu] = useState(false); //mobile only

	useEffect(() => {}, []);

	useEffect(() => {
		// anytime path changes, close nav & subnav
		setShowMenu(false);
	}, [router.asPath]);

	return (
		<nav>
			<div className="mobile-toggle" onClick={() => setShowMenu(!showMenu)}>
				Menu &#9776;
			</div>
			<ul className={showMenu ? "menu-active" : ""}>
				<li>
					<Link href="/">
						<a>Link</a>
					</Link>
				</li>
				<li>
					<Link href="/">
						<a>Link</a>
					</Link>
				</li>
				<li>
					<Link href="/">
						<a>Link</a>
					</Link>
				</li>
				<li>
					<Link href="/">
						<a>Link</a>
					</Link>
				</li>
				<li>
					<Link href="/">
						<a>Link</a>
					</Link>
				</li>
			</ul>
		</nav>
	);
};

export default Nav;
