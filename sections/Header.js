import { useEffect, useState } from "react";

import Link from "../components/routing/Link";

import Nav from "./Nav";

const Header = () => {
	const [scrolled, setScrolled] = useState(false);

	useEffect(() => {
		window.addEventListener("scroll", () => {
			setScrolled(document.documentElement.scrollTop > 100);
		});
	}, []);

	return (
		<header className={scrolled ? "sticky scroll" : "sticky"}>
			<div className="topbar">
				<div className="cont">
					<ul>
						<li>
							<a href="" target="_blank">
								Link
							</a>
						</li>
						<li>
							<a href="" target="_blank">
								Link
							</a>
						</li>
						<li>
							<a href="" target="_blank">
								Link
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div className="main-header">
				<div class="cont">
					<Link href="/" className="logo">
						<img src="images/logo.png" alt="logo" />
					</Link>
					<Nav />
				</div>
			</div>
		</header>
	);
};

export default Header;
