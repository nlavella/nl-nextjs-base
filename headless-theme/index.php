<?php
/**
 * Redirect theme requests to frontend.
 *
 * @author NL
 * @since 1.0
 */

header( 'Location:' . HEADLESS_FRONTEND_URL, true, 303 );
